import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginActivateGuard } from './guards/login-activate/login-activate.guard';
import { LogoutActivateGuard } from './guards/logout-activate/logout-activate.guard';

const routes: Routes = [
  { path: 'restaurants',
   canActivate: [LoginActivateGuard],
   loadChildren: './restaurants/restaurants.module#RestaurantsModule'
  },
  {
    path: 'auth',
    canActivate: [LogoutActivateGuard],
    loadChildren: './auth/auth.module#AuthModule'
  },
  {
    path: 'users',
    canActivate: [LoginActivateGuard],
    loadChildren: './users/users.module#UsersModule'
  },
  { path: '', redirectTo: '/auth/login', pathMatch: 'full' },
  { path: '**', redirectTo: '/auth/login', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
