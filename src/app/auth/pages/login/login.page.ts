import { Component, OnInit } from '@angular/core';
import { Iuser } from '../../interfaces/iuser';
import { AuthService } from '../../services/auth.service';
import { NavController, AlertController, Platform } from '@ionic/angular';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { Facebook, FacebookLoginResponse } from '@ionic-native/facebook/ngx';
import { ILoginGoogleFbRequest } from '../../interfaces/ilogin-google-fb-request';
import { GooglePlus } from '@ionic-native/google-plus/ngx';
import { OneSignal } from '@ionic-native/onesignal/ngx';


@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  user: Iuser;
  peticionLoginGoogleFB: ILoginGoogleFbRequest;
  accessToken = '';
  response = null;

  constructor(private authService: AuthService,
     private nav: NavController,
     private alertCtrl: AlertController,
     private geolocation: Geolocation,
     public fb: Facebook,
     public gplus: GooglePlus,
     private oneSignal: OneSignal,
     private platform: Platform
  ) { }

  ngOnInit() {
    this.resetForm();
    this.geolocate();
  }

  async login() {
    try {
      const oneSignalId = (await this.oneSignal.getIds()).userId;
      this.user.oneSignalId = oneSignalId;
    } catch (e) {
      console.log(e);
    }

    console.log('haciendo login');


    this.authService.login(this.user).subscribe(
      () => this.nav.navigateBack(['/restaurants']),
      async error => {
        (await this.alertCtrl.create({
          header: 'Login error',
          message: 'Incorrect email and/or password',
          buttons: ['Ok']
        })).present();
      }
    );

    console.log(this.user);
  }

  async loginGoogle() {
    try {
      const res = await this.gplus.login({'webClientId': '83592756611-cd5bhd8g7le8uq6q28ql85vpjpf209a6.apps.googleusercontent.com'});
        this.response = res;
        console.log(res);
        this.peticionLoginGoogleFB = {
          token: this.response.idToken,
          lat: this.user.lat,
          lng: this.user.lng
        };
        console.log(this.peticionLoginGoogleFB);
        this.authService.loginGoogle(this.peticionLoginGoogleFB).subscribe(
          us => this.nav.navigateRoot(['/restaurants']),
          async error => {
            (await this.alertCtrl.create({
              header: 'Login error',
              message: 'Try again',
              buttons: ['Ok']
            })).present();
          }
        );
      } catch (err) {
        console.error(err);
      }
  }

  async loginFB() {
    const resp = await this.fb.login(['public_profile', 'email']);
    if (resp.status === 'connected') {
      this.accessToken = resp.authResponse.accessToken;
      this.peticionLoginGoogleFB = {
        token: this.accessToken,
        lat: this.user.lat,
        lng: this.user.lng
      };
      this.authService.loginFacebook(this.peticionLoginGoogleFB).subscribe(
        us => this.nav.navigateRoot(['/restaurants']),
        async error => {
          (await this.alertCtrl.create({
            header: 'Login error',
            message: 'Try again',
            buttons: ['Ok']
          })).present();
        }
      );
    }
  }

  geolocate() {
    this.geolocation.getCurrentPosition().then((data) => {
      this.user.lat = data.coords.latitude;
      this.user.lng = data.coords.longitude;
    }).catch((error) => {
      console.log('Error getting location', error);
    });
  }

  resetForm() {
    this.user = {
      email: '',
      password: '',
      lat: 0,
      lng: 0
    };
  }

}
