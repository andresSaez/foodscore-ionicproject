import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { ToastController, NavController } from '@ionic/angular';
import { Iuser } from '../../interfaces/iuser';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { Geolocation } from '@ionic-native/geolocation/ngx';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {
  newUser: Iuser;
  password2: string;

  constructor(
    private authService: AuthService,
    private toast: ToastController,
    private nav: NavController,
    private camera: Camera,
    private geolocation: Geolocation
  ) { }

  ngOnInit() {
    this.resetForm();
    this.geolocate();
  }

  register() {
    this.authService.register(this.newUser).subscribe(
      async () => {
        (await this.toast.create({
          duration: 3000,
          position: 'bottom',
          message: 'User registered!'
        })).present();
        this.nav.navigateRoot(['/auth/login']);
      }
    );
  }

  async takePhoto() {
    const options: CameraOptions = {
      targetWidth: 640, // max width 640px
      targetHeight: 640, // max height 640px
      allowEdit: true,
      destinationType: this.camera.DestinationType.DATA_URL // Base64
    };

    await this.getPicture(options);
  }

  async pickFromGallery() {
    const options: CameraOptions = {
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      targetWidth: 640, // max width 640px
      targetHeight: 640, // max height 640px
      allowEdit: true,
      destinationType: this.camera.DestinationType.DATA_URL // Base64
    };

    await this.getPicture(options);
  }

  private async getPicture(options: CameraOptions) {
    const imageData = await this.camera.getPicture(options);
    this.newUser.avatar = 'data:image/jpeg;base64,' + imageData;
  }

  geolocate() {
    this.geolocation.getCurrentPosition().then((data) => {
      this.newUser.lat = data.coords.latitude;
      this.newUser.lng = data.coords.longitude;
    }).catch((error) => {
      console.log('Error getting location', error);
    });
  }

  resetForm() {
    this.newUser = {
      name: '',
      avatar: '',
      email: '',
      password: '',
      lat: 0,
      lng: 0
    };

    this.password2 = '';
  }

}
