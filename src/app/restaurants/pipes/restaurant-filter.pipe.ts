import { Pipe, PipeTransform } from '@angular/core';
import { Irestaurant } from '../interfaces/irestaurant';

@Pipe({
  name: 'restaurantFilter'
})
export class RestaurantFilterPipe implements PipeTransform {

  transform(restaurants: Irestaurant[], orderByName: boolean, showOpen: boolean): any {
    let restFiltered = restaurants;
    if (orderByName) {
      restFiltered = this.orderRestName(restFiltered);
    }
    if (showOpen) {
      restFiltered = this.filterOpen(restFiltered);
    }
    return restFiltered;
  }

  private orderRestName(rests: Irestaurant[]): Irestaurant[] {
    return rests.sort((r1, r2) => r1.name.localeCompare(r2.name));
  }

  private filterOpen(rests: Irestaurant[]): Irestaurant[] {
    const today = new Date().getDay();
    return rests.filter(r => r.daysOpen.includes(today));
  }

  private filterName(rests: Irestaurant[], search: string): Irestaurant[] {
    if (search !== '') {
      return rests.filter(r => r.name.toLocaleLowerCase().includes(search.toLocaleLowerCase()));
    } else {
      return rests;
    }
  }
}
