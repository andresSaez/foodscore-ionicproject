import { Injectable } from '@angular/core';
import { Resolve, Router, ActivatedRouteSnapshot } from '@angular/router';
import { Irestaurant } from '../interfaces/irestaurant';
import { RestaurantService } from '../services/restaurant.service';
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class RestaurantResolverService implements Resolve<Irestaurant> {

  constructor(private restaurantService: RestaurantService,
    private router: Router) { }

  resolve(route: ActivatedRouteSnapshot): Observable<Irestaurant> {
    return this.restaurantService.getRestaurant(route.params['id']).pipe(
      catchError(error => { this.router.navigate(['/restaurants']);
        return of(null);
      })
    );
  }
}
