import { Component, OnInit } from '@angular/core';
import { RestaurantService } from '../../services/restaurant.service';
import { ActivatedRoute } from '@angular/router';
import { Irestaurant } from '../../interfaces/irestaurant';
import { Show } from '../../enums/enum-show.enum';
import { PopoverController } from '@ionic/angular';
import { ChooseFilterComponent } from './popover/choose-filter/choose-filter.component';

@Component({
  selector: 'app-restaurant-list',
  templateUrl: './restaurant-list.page.html',
  styleUrls: ['./restaurant-list.page.scss'],
})
export class RestaurantListPage implements OnInit {
  restaurants: Irestaurant[];
  showOpen = false;
  orderByName = false;
  num = 0;

  filteredItems: Irestaurant[] = [];

  constructor(
    private restaurantService: RestaurantService,
    private route: ActivatedRoute,
    private popoverCtrl: PopoverController
    ) {
    // this.loadMoreItems(null);
  }

  ngOnInit() {}

  ionViewWillEnter() {
    const id = +this.route.snapshot.params.id;
    const show = this.route.snapshot.data.show;

    if (show === Show.ALL) {
      this.restaurantService.getRestaurants().subscribe(
        (restaurants) => {
          this.restaurants = restaurants;
          this.resetearLista();
        },
        error => console.log(error),
        () => console.log('Restaurants loaded')
      );
    }

    if (show === Show.MINE) {
      this.restaurantService.getRestaurantsMine().subscribe(
        (restaurants) => {
          this.restaurants = restaurants;
          this.resetearLista();
        },
        error => console.log(error),
        () => console.log('Restaurants loaded')
      );
    }

    if (show === Show.ASSIST) {
      this.restaurantService.getRestaurantsFromUser(id).subscribe(
        (restaurants) => {
          this.restaurants = restaurants;
          this.resetearLista();
        },
        error => console.log(error),
        () => console.log('Restaurants loaded')
      );
    }
    /*
    const id = +this.route.snapshot.params.id;
    const show = this.route.snapshot.data.show;

    if (show === Show.ALL) {
      this.restaurantService.getRestaurants().subscribe(
        (restaurants) => {
          this.restaurants = restaurants;
          this.filteredItems = this.restaurants;
        },
        error => console.log(error),
        () => console.log('Restaurants loaded')
      );
    }

    if (show === Show.MINE) {
      this.restaurantService.getRestaurantsMine().subscribe(
        (restaurants) => {
          this.restaurants = restaurants;
          this.filteredItems = this.restaurants;
        },
        error => console.log(error),
        () => console.log('Restaurants loaded')
      );
    }

    if (show === Show.ASSIST) {
      this.restaurantService.getRestaurantsFromUser(id).subscribe(
        (restaurants) => {
          this.restaurants = restaurants;
          this.filteredItems = this.restaurants;
        },
        error => console.log(error),
        () => console.log('Restaurants loaded')
      );
    }
    */
  }

  filterItems(event) {
    let search: string = event.target.value;
    if (search && search.trim() !== '') {
      search = search.trim().toLowerCase();
      this.filteredItems = this.restaurants.filter(i =>
        i.name.toLowerCase().includes(search)
      );
    } else {
        this.resetearLista();
    }
  }

  refresh(event) {
    const id = +this.route.snapshot.params.id;
    const show = this.route.snapshot.data.show;

    if (show === Show.ALL) {
      this.restaurantService.getRestaurants().subscribe(
        (restaurants) => {
          this.restaurants = restaurants;
          this.resetearLista();
          event.target.complete();
        });
    }

    if (show === Show.MINE) {
      this.restaurantService.getRestaurantsMine().subscribe(
        (restaurants) => {
          this.restaurants = restaurants;
          this.resetearLista();
          event.target.complete();
        });
    }

    if (show === Show.ASSIST) {
      this.restaurantService.getRestaurantsFromUser(id).subscribe(
        (restaurants) => {
          this.restaurants = restaurants;
          this.resetearLista();
          event.target.complete();
        });
    }
  }

  loadMoreItems(event) {
    const max = this.num + 3;
    for (; this.num < max; this.num++) {
      if (this.num <= (this.restaurants.length - 1)) {
        this.filteredItems.push(this.restaurants[this.num]);
      }
    }

    if (this.filterItems.length === this.restaurants.length) {
      event.target.disabled = true;
    }
    if (event != null) {
      event.target.complete();
    }
  }

  async showPopover(event) {
    const popover = await this.popoverCtrl.create({
      component: ChooseFilterComponent,
      componentProps: {
        title: 'Filter restaurants by:'
      },
      event: event
    });

    await popover.present();
    const result = await popover.onDidDismiss();
    if (result.data === 'showOpen') {
      this.showOpen = !this.showOpen;
    }
    if (result.data === 'orderByName') {
      this.orderByName = !this.orderByName;
    }
  }

  resetearLista() {
    this.num = 0;
    this.filteredItems = [];
    this.loadMoreItems(null);
  }

}
