import { Component, OnInit, Input } from '@angular/core';
import { PopoverController } from '@ionic/angular';

@Component({
  selector: 'app-choose-filter',
  template: `
    <ion-content>
      <ion-list>
        <ion-list-header><strong>{{title}}</strong></ion-list-header>
        <ion-item (click)="close('showOpen')">
          <ion-label>View only open</ion-label>
        </ion-item>
        <ion-item (click)="close('orderByName')" lines="none">
          <ion-label>Order by name</ion-label>
        </ion-item>
      </ion-list>
    </ion-content>
    `
})
export class ChooseFilterComponent {
  @Input() title;

  constructor(public popoverCtrl: PopoverController) { }

  close(filtro: string) {
      this.popoverCtrl.dismiss(filtro);
  }
}
