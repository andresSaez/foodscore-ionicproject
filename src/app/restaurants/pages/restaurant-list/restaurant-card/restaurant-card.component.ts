import { Component, OnInit, Input } from '@angular/core';
import { Irestaurant } from 'src/app/restaurants/interfaces/irestaurant';

@Component({
  selector: 'app-restaurant-card',
  templateUrl: './restaurant-card.component.html',
  styleUrls: ['./restaurant-card.component.scss']
})
export class RestaurantCardComponent implements OnInit {
  @Input() restaurant: Irestaurant;
  weekDay: number;
  readonly days = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
  fullStars: number[];
  emptyStars: number[];

  constructor() { }

  ngOnInit() {
    this.weekDay = new Date().getDay();
    this.restaurant.daysOpen = this.restaurant.daysOpen.map(d => +d);
    this.fullStars = (new Array(Math.round(this.restaurant.stars))).fill(1);
    this.emptyStars = (new Array(5 - Math.round(this.restaurant.stars))).fill(1);
    // this.restaurant.image.replace('\\', '/');
  }

}
