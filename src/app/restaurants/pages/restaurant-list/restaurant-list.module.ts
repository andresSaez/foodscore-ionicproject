import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { RestaurantListPage } from './restaurant-list.page';
import { ChooseFilterComponent } from './popover/choose-filter/choose-filter.component';
import { RestaurantFilterPipe } from '../../pipes/restaurant-filter.pipe';
import { RestaurantCardComponent } from './restaurant-card/restaurant-card.component';

const routes: Routes = [
  {
    path: '',
    component: RestaurantListPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
  ],
  declarations: [RestaurantListPage, ChooseFilterComponent, RestaurantFilterPipe, RestaurantCardComponent],
  entryComponents: [ChooseFilterComponent]
})
export class RestaurantListPageModule {}
