import { Component, OnInit, Input, AfterViewInit, ViewChild } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { LngLat } from 'mapbox-gl';
import { Result } from 'ngx-mapbox-gl/lib/control/geocoder-control.directive';
import { MapComponent } from 'ngx-mapbox-gl';

@Component({
  selector: 'app-select-position',
  templateUrl: './select-position.component.html',
  styleUrls: ['./select-position.component.scss']
})
export class SelectPositionComponent implements OnInit, AfterViewInit {
  zoom = 17;
  @Input() lat;
  @Input() lng;
  direccion = {
    lat: 0,
    lng: 0,
    address: ''
  };
  @ViewChild(MapComponent) mapComp: MapComponent;

  constructor(
    public modalCtrl: ModalController
  ) { }

  ngOnInit() {
    this.direccion = {
      lat: this.lat,
      lng: this.lng,
      address: ''
    };
  }

  ngAfterViewInit() {
    this.mapComp.load.subscribe(
    () => {
    console.log(this.mapComp);
    console.log(this.mapComp.mapInstance);
    this.mapComp.mapInstance.resize(); // Necessary for full height
    }
    );
  }

  changePosition(result: Result) {
    this.direccion.lat = result.geometry.coordinates[1];
    this.direccion.lng = result.geometry.coordinates[0];
    this.direccion.address = result.place_name;
    console.log('New address: ' + result.place_name);
  }

  selectPosition() {
    this.modalCtrl.dismiss(this.direccion);
  }

  cancel() {
    this.modalCtrl.dismiss(this.direccion = {
      lat: this.lat,
      lng: this.lng,
      address: ''
    });
  }
}
