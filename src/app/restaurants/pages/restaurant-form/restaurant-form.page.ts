import { Component, OnInit, ViewChild } from '@angular/core';
import { RestaurantService } from '../../services/restaurant.service';
import { ToastController, NavController, ModalController, AlertController } from '@ionic/angular';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { Irestaurant } from '../../interfaces/irestaurant';
import { ActivatedRoute } from '@angular/router';
import { Result } from 'ngx-mapbox-gl/lib/control/geocoder-control.directive';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { NgModel, NgForm } from '@angular/forms';
import { SelectPositionComponent } from './modals/select-position/select-position.component';

@Component({
  selector: 'app-restaurant-form',
  templateUrl: './restaurant-form.page.html',
  styleUrls: ['./restaurant-form.page.scss'],
})
export class RestaurantFormPage implements OnInit {
  newRest: Irestaurant;
  zoom = 10;
  daysOpen: boolean[] = (new Array(7)).fill(true);
  weekDay: number;
  cuisine: string;
  edit = false;
  guardado = false;
  readonly days = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
  @ViewChild('addForm') addForm: NgForm;

  constructor(
    private restaurantService: RestaurantService,
    private route: ActivatedRoute,
    private toast: ToastController,
    private nav: NavController,
    private camera: Camera,
    private geolocation: Geolocation,
    private modalCtrl: ModalController,
    private alertCtrl: AlertController
  ) { }

  ngOnInit() {
    const id = +this.route.snapshot.params.id;
    this.weekDay = new Date().getDay();
    this.resetForm();
    if (!isNaN(id)) {
      this.edit = true;
      this.restaurantService.getRestaurant(id).subscribe(
        restau => {
          this.newRest = restau;
          this.cuisine = this.newRest.cuisine.join(', ');
          this.newRest.daysOpen = this.newRest.daysOpen.map(d => +d);
          this.daysOpen = this.daysOpen.map((day, i) => this.newRest.daysOpen.includes(i) ? true : false);
        }
      );
    } else {
      this.geolocate();
    }
  }

  addRestaurant() {
    this.prepararRestauranteParaGuardar();
    if (!this.edit) {
      this.restaurantService.addRestaurant(this.newRest).subscribe(
        async () => {
          (await this.toast.create({
            duration: 3000,
            position: 'bottom',
            message: 'Restaurant created!'
          })).present();
          this.nav.navigateRoot(['/restaurants']);
        },
        error => console.log(error)
      );
    } else {
      console.log(this.newRest);
      this.restaurantService.updateRestaurant(this.newRest).subscribe(
        async () => {
          (await this.toast.create({
            duration: 3000,
            position: 'bottom',
            message: 'Restaurant updated!'
          })).present();
          this.guardado = true;
          this.nav.navigateRoot(['/restaurants']);
        }
      );
    }
  }

  prepararRestauranteParaGuardar() {
    this.newRest.daysOpen = this.daysOpen.reduce((days, isSelected, i) => isSelected ? [...days, i] : days, []);
    this.newRest.cuisine = this.cuisine.split(',');
    this.newRest.lat = +this.newRest.lat;
    this.newRest.lng = +this.newRest.lng;
    delete this.newRest.commented;
    delete this.newRest.mine;
    delete this.newRest.creator;
    delete this.newRest.distance;

    if (this.newRest.image.includes('restaurants')) {
      this.newRest.image = this.newRest.image.replace('img\\', 'img/');
    }
  }

  async takePhoto() {
    const options: CameraOptions = {
      targetWidth: 640, // max width 640px
      targetHeight: 640, // max height 640px
      allowEdit: true,
      destinationType: this.camera.DestinationType.DATA_URL // Base64
    };

    await this.getPicture(options);
  }

  async pickFromGallery() {
    const options: CameraOptions = {
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      targetWidth: 640, // max width 640px
      targetHeight: 640, // max height 640px
      allowEdit: true,
      destinationType: this.camera.DestinationType.DATA_URL // Base64
    };

    await this.getPicture(options);
  }

  private async getPicture(options: CameraOptions) {
    const imageData = await this.camera.getPicture(options);
    this.newRest.image = 'data:image/jpeg;base64,' + imageData;
  }

  resetForm() {
    this.newRest = {
      name: '',
      image: '',
      cuisine: [],
      description: '',
      phone: '',
      daysOpen: [],
      lat: 0,
      lng: 0,
      address: ''
    };
  }

  geolocate() {
    this.geolocation.getCurrentPosition().then((data) => {
      this.newRest.lat = data.coords.latitude;
      this.newRest.lng = data.coords.longitude;
    }).catch((error) => {
      console.log('Error getting location', error);
    });
  }

  async openSelectAddress() {
    const modal = await this.modalCtrl.create({
      component: SelectPositionComponent,
      componentProps: { lat: this.newRest.lat, lng: this.newRest.lng }
    });

    await modal.present();

    const result = await modal.onDidDismiss();
    this.newRest.lat = result.data.lat;
    this.newRest.lng = result.data.lng;
    this.newRest.address = result.data.address;
  }
}
