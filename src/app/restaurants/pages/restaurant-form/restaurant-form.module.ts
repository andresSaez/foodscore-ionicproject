import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { RestaurantFormPage } from './restaurant-form.page';
import { SharedModule } from 'src/app/shared/shared.module';
import { SelectPositionComponent } from './modals/select-position/select-position.component';
import { NgxMapboxGLModule } from 'ngx-mapbox-gl';

const routes: Routes = [
  {
    path: '',
    component: RestaurantFormPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    SharedModule,
    NgxMapboxGLModule
  ],
  declarations: [RestaurantFormPage, SelectPositionComponent],
  entryComponents: [SelectPositionComponent]
})
export class RestaurantFormPageModule {}
