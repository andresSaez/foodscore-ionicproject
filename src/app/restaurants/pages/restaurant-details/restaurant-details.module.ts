import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { RestaurantDetailsPage } from './restaurant-details.page';
import { NgxMapboxGLModule } from 'ngx-mapbox-gl';

const routes: Routes = [
  {
    path: '',
    component: RestaurantDetailsPage,
    children: [
      { path: 'info', loadChildren: './restaurant-info/restaurant-info.module#RestaurantInfoPageModule' },
      { path: 'location', loadChildren: './restaurant-location/restaurant-location.module#RestaurantLocationPageModule' },
      { path: 'comments', loadChildren: './restaurant-comments/restaurant-comments.module#RestaurantCommentsPageModule' },
      { path: '', pathMatch: 'full', redirectTo: 'info'}
    ]
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [RestaurantDetailsPage]
})
export class RestaurantDetailsPageModule {}
