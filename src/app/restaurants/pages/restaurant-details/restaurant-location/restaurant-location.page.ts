import { Component, OnInit, AfterViewInit, ViewChild } from '@angular/core';
import { MapComponent } from 'ngx-mapbox-gl';
import { LaunchNavigator, LaunchNavigatorOptions} from '@ionic-native/launch-navigator/ngx';
import { Events } from '@ionic/angular';
import { Irestaurant } from 'src/app/restaurants/interfaces/irestaurant';

@Component({
  selector: 'app-restaurant-location',
  templateUrl: './restaurant-location.page.html',
  styleUrls: ['./restaurant-location.page.scss'],
})
export class RestaurantLocationPage implements OnInit, AfterViewInit {
  zoom = 17;
  restaurant: Irestaurant;
  @ViewChild(MapComponent) mapComp: MapComponent;

  constructor(private launchNavigator: LaunchNavigator,
    private events: Events) { }

  ngAfterViewInit() {
    this.mapComp.load.subscribe(
      () => {
        console.log(this.mapComp);
        console.log(this.mapComp.mapInstance);
        this.mapComp.mapInstance.resize(); // Necessary for full height
      }
    );
  }

  ngOnInit() {
    this.events.subscribe('restaurant', rest => {
      this.restaurant = rest;
    });
  }

  async navigate() {
    const options: LaunchNavigatorOptions = {};

    await this.launchNavigator.navigate([this.restaurant.lat, this.restaurant.lng], options);
  }
}
