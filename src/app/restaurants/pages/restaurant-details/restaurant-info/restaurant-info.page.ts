import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Irestaurant } from 'src/app/restaurants/interfaces/irestaurant';
import { Events, AlertController, NavController } from '@ionic/angular';
import { RestaurantService } from 'src/app/restaurants/services/restaurant.service';

@Component({
  selector: 'app-restaurant-info',
  templateUrl: './restaurant-info.page.html',
  styleUrls: ['./restaurant-info.page.scss'],
})
export class RestaurantInfoPage implements OnInit {
  restaurant: Irestaurant;
  weekDay: number;
  readonly days = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
  fullStars: number[];
  emptyStars: number[];

  constructor(
    private events: Events,
    private alertCtrl: AlertController,
    private restaurantService: RestaurantService,
    private nav: NavController,
  ) { }

  ngOnInit() {
    this.weekDay = new Date().getDay();
    this.events.subscribe('restaurant', rest => {
      this.restaurant = rest;
      this.restaurant.daysOpen = this.restaurant.daysOpen.map(d => +d);
      this.fullStars = (new Array(Math.round(this.restaurant.stars))).fill(1);
      this.emptyStars = (new Array(5 - Math.round(this.restaurant.stars))).fill(1);
    });
  }

  async delete() {
    const alert = await this.alertCtrl.create({
      header: 'Delete restaurant',
      message: 'Are you sure want to delete this restaurant?',
      buttons: [
        {
          text: 'Ok',
          handler: () => {
            this.restaurantService
            .deleteRestaurant(this.restaurant.id)
            .subscribe(() => {
              this.nav.navigateRoot(['/restaurants']);
            });
          }
        },
        {
          text: 'Cancel',
          role: 'cancel'
        }
      ]
    });
    alert.present();
  }
}
