import { Component, OnInit, Input } from '@angular/core';
import { IComment } from 'src/app/restaurants/interfaces/icomment';

@Component({
  selector: 'app-comment-component',
  templateUrl: './comment-component.component.html',
  styleUrls: ['./comment-component.component.scss']
})
export class CommentComponentComponent implements OnInit {
  @Input() com: IComment;
  fullStars: number[];
  emptyStars: number[];

  constructor() { }

  ngOnInit() {
    this.fullStars = (new Array(Math.round(this.com.stars))).fill(1);
    this.emptyStars = (new Array(5 - Math.round(this.com.stars))).fill(1);
  }
}
