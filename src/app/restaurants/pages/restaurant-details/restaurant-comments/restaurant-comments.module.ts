import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { RestaurantCommentsPage } from './restaurant-comments.page';
import { CommentComponentComponent } from './comment-component/comment-component.component';

const routes: Routes = [
  {
    path: '',
    component: RestaurantCommentsPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [RestaurantCommentsPage, CommentComponentComponent]
})
export class RestaurantCommentsPageModule {}
