import { Component, OnInit, NgZone } from '@angular/core';
import { RestaurantService } from 'src/app/restaurants/services/restaurant.service';
import { ActivatedRoute } from '@angular/router';
import { IComment } from 'src/app/restaurants/interfaces/icomment';
import { AlertController, Events } from '@ionic/angular';

@Component({
  selector: 'app-restaurant-comments',
  templateUrl: './restaurant-comments.page.html',
  styleUrls: ['./restaurant-comments.page.scss'],
})
export class RestaurantCommentsPage implements OnInit {
  idRest: number;
  comments: IComment[];
  newComment: IComment;
  commented: boolean;

  constructor(private restaurantService: RestaurantService,
    private route: ActivatedRoute,
    private ngZone: NgZone,
    private alertCtrl: AlertController,
    private events: Events) { }

  ngOnInit() {
    this.idRest = +this.route.snapshot.parent.parent.params.id;
    this.restaurantService.getCommentsFromRestaurant(this.idRest).subscribe(
      resp => this.comments = resp,
      error => console.log(error),
      () => console.log('Comments loaded')
    );
    this.events.subscribe('restaurant', rest => {
      this.commented = rest.commented;
      console.log(rest.commented);
    });
    this.newComment = {
      text: '',
      stars: 0,
    };
  }

  async addComment() {
    const alert = await this.alertCtrl.create({
      header: 'New comment',
      inputs: [
        {
          name: 'comment',
          type: 'text',
          placeholder: 'Enter your comment'
        },
        {
          name: 'score',
          type: 'number',
          placeholder: 'Score 1 to 5'
        }
      ],
      buttons: [
        {
          text: 'Add',
          role: 'ok'
        },
        {
          role: 'cancel',
          text: 'Cancel'
        }
      ]
    });

    await alert.present();
    const result = await alert.onDidDismiss();

    if (result.role === 'ok') {
      this.newComment = {
        text: result.data.values.comment,
        stars: +result.data.values.score
      };
      this.restaurantService
        .addComment(this.newComment, this.idRest)
        .subscribe(com => this.ngZone.run(() => {
          this.comments.push(com);
          this.commented = true;
        }));
    }
  }
}
