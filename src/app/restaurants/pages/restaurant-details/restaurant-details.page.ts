import { Component, OnInit } from '@angular/core';
import { Irestaurant } from '../../interfaces/irestaurant';
import { ActivatedRoute } from '@angular/router';
import { Events } from '@ionic/angular';

@Component({
  selector: 'app-restaurant-details',
  templateUrl: './restaurant-details.page.html',
  styleUrls: ['./restaurant-details.page.scss'],
})
export class RestaurantDetailsPage implements OnInit {
  restaurant: Irestaurant;

  constructor(private route: ActivatedRoute, private events: Events) { }

  ngOnInit() {
    this.restaurant = this.route.snapshot.data.restaurant;
  }

  changeTab(event) {
    this.events.publish('restaurant', this.restaurant);
  }

}
