import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { Irestaurant } from '../interfaces/irestaurant';
import { IComment } from '../interfaces/icomment';

@Injectable({
  providedIn: 'root'
})
export class RestaurantService {
  readonly BASE_URL = 'restaurants';

  constructor(private http: HttpClient) { }

  getRestaurants(): Observable<Irestaurant[]> {
    return this.http.get<{restaurants: Irestaurant[]}>(this.BASE_URL)
      .pipe(map(resp => resp.restaurants.map(r => {
        r.image = environment.baseUrl + '/' + r.image;
        return r;
      })));
  }

  getRestaurantsMine(): Observable<Irestaurant[]> {
    return this.http.get<{restaurants: Irestaurant[]}>(`${this.BASE_URL}/mine`)
      .pipe(map(resp => resp.restaurants.map(r => {
        r.image = environment.baseUrl + '/' + r.image;
        return r;
      })));
  }

  getRestaurantsFromUser(id: number): Observable<Irestaurant[]> {
    return this.http.get<{restaurants: Irestaurant[]}>(`${this.BASE_URL}/user/${id}`)
      .pipe(map(resp => resp.restaurants.map(r => {
        r.image = environment.baseUrl + '/' + r.image;
        return r;
      })));
  }

  getRestaurant(id: number): Observable<Irestaurant> {
    return this.http.get<{restaurant: Irestaurant}>(`${this.BASE_URL}/${id}`)
      .pipe(
        map(resp => {
          const r = resp.restaurant;
          r.image = environment.baseUrl + '/' + r.image;
          r.creator.avatar = environment.baseUrl + '/' + r.creator.avatar;
          return r;
        })
      );
  }

  addRestaurant(rest: Irestaurant): Observable<Irestaurant> {
    return this.http.post<{restaurant: Irestaurant}>(this.BASE_URL, rest)
    .pipe(map(resp => {
      const r = resp.restaurant;
      r.image = environment.baseUrl + '/' + r.image;
      return r;
    }));
  }

  updateRestaurant(rest: Irestaurant): Observable<Irestaurant> {
    return this.http.put<{restaurant: Irestaurant}>(`${this.BASE_URL}/${rest.id}`, rest)
    .pipe(map(resp => {
      const r = resp.restaurant;
      r.image = environment.baseUrl + '/' + r.image;
      return r;
    }));
  }

  deleteRestaurant(id: number): Observable<void> {
    return this.http.delete<{restaurant: Irestaurant}>(`${this.BASE_URL}/${id}`)
      .pipe(
        map(resp => {
          return;
        })
      );
  }

  getCommentsFromRestaurant(id: Number): Observable<IComment[]> {
    return this.http.get<{comments: IComment[]}>(`${this.BASE_URL}/${id}/comments`)
    .pipe(map(resp => resp.comments.map(c => {
      c.user.avatar = environment.baseUrl + '/' + c.user.avatar;
      return c;
    })));
  }

  addComment(newComment: IComment, id: Number): Observable<IComment> {
    return this.http.post<{comment: IComment}>(`${this.BASE_URL}/${id}/comments`, newComment )
      .pipe(map(resp => {
        const c = resp.comment;
        c.user.avatar = environment.baseUrl + '/' + c.user.avatar;
        return c;
      }));
  }
}
