import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RestaurantResolverService } from './resolvers/restaurant-resolver.service';
import { Show } from './enums/enum-show.enum';

const routes: Routes = [
  {
    path: '',
    loadChildren: './pages/restaurant-list/restaurant-list.module#RestaurantListPageModule',
    pathMatch: 'full',
    data: { show: Show.ALL }
  },
  {
    path: 'details/:id',
    loadChildren: './pages/restaurant-details/restaurant-details.module#RestaurantDetailsPageModule',
    resolve: { restaurant: RestaurantResolverService }
  },
  {
    path: 'add',
    loadChildren: './pages/restaurant-form/restaurant-form.module#RestaurantFormPageModule'
  },
  {
    path: 'edit/:id',
    loadChildren: './pages/restaurant-form/restaurant-form.module#RestaurantFormPageModule'
  },
  {
    path: 'mine',
    loadChildren: './pages/restaurant-list/restaurant-list.module#RestaurantListPageModule',
    data: { show: Show.MINE }
  },
  {
    path: 'user/:id',
    loadChildren: './pages/restaurant-list/restaurant-list.module#RestaurantListPageModule',
    data: { show: Show.ASSIST }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RestaurantsRoutingModule { }
