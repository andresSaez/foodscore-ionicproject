import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatchDirective } from './validators/match-validator/match.directive';
import { OneCheckedDirective } from './validators/one-checked-validator/one-checked.directive';

@NgModule({
  declarations: [MatchDirective, OneCheckedDirective],
  imports: [
    CommonModule
  ],
  exports: [
    MatchDirective,
    OneCheckedDirective
  ]
})
export class SharedModule { }
