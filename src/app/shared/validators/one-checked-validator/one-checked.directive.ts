import { Directive } from '@angular/core';
import { Validator, AbstractControl, FormGroup, NG_VALIDATORS } from '@angular/forms';

@Directive({
  selector: '[appOneChecked]'
})
export class OneCheckedDirective implements Validator {

  constructor() { }

  validate(group: AbstractControl): { [key: string]: any} {
    if (group instanceof FormGroup) {
      if (Object.values(group.value).every(v => v === false)) { // No checked
        return { 'oneChecked': true };
      }
    }

    return null;
  }

}
