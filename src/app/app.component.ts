import { Component } from '@angular/core';

import { Platform, NavController, ToastController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { AuthService } from './auth/services/auth.service';
import { OneSignal } from '@ionic-native/onesignal/ngx';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html'
})
export class AppComponent {
  menuDisabled = true;
  public appPages = [
    {
      title: 'Profile',
      url: '/users/profile',
      icon: 'person'
    },
    {
      title: 'Restaurants',
      url: '/restaurants',
      icon: 'restaurant'
    },
    {
      title: 'New restaurant',
      url: '/restaurants/add',
      icon: 'add'
    }
  ];

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private authService: AuthService,
    private nav: NavController,
    private oneSignal: OneSignal,
    private toastCtrl: ToastController
  ) {
    this.initializeApp();
    this.authService.loginChange$.subscribe(
      loggued => this.menuDisabled = !loggued
    );
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
      this.oneSignal.startInit('e75e1da6-dc25-4d45-9ea6-052ff5d83027', '83592756611');
      this.oneSignal.inFocusDisplaying(this.oneSignal.OSInFocusDisplayOption.None);

      this.oneSignal.handleNotificationOpened().subscribe(
        notif => {
          if (notif.notification.payload.additionalData.restId) {
            console.log(notif.notification.payload.additionalData.restId);
            this.nav.navigateForward([`/restaurants/details/${notif.notification.payload.additionalData.restId}/comments`]);
          }
        }
      );
      this.oneSignal.handleNotificationReceived().subscribe(
        async notif => {
          console.log('estoy en handleReceived:' + notif);
          if (!notif.shown) { // The user did not see the notification
            const toast = await this.toastCtrl.create({
              duration: 2000,
              position: 'bottom',
              message: notif.payload.title
            });
            toast.present();
          }
        }
      );
      this.oneSignal.endInit();
    });
  }

  async logout() {
    await this.authService.logout();
    this.nav.navigateRoot(['/auth/login']);
  }
}
