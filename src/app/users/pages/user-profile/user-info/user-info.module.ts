import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { UserInfoPage } from './user-info.page';
import { ChangeEmailComponent } from './modals/change-email/change-email.component';
import { ChangePasswordComponent } from './modals/change-password/change-password.component';
import { SharedModule } from 'src/app/shared/shared.module';

const routes: Routes = [
  {
    path: '',
    component: UserInfoPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    SharedModule
  ],
  declarations: [UserInfoPage, ChangeEmailComponent, ChangePasswordComponent],
  entryComponents: [ChangeEmailComponent, ChangePasswordComponent]
})
export class UserInfoPageModule {}
