import { Component, OnInit } from '@angular/core';
import { ModalController, ToastController } from '@ionic/angular';
import { UsersService } from 'src/app/users/services/users.service';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.scss']
})
export class ChangePasswordComponent implements OnInit {
  password: string;
  password2: string;

  constructor(
    public modalCtrl: ModalController,
    public userServ: UsersService,
    public toast: ToastController
  ) { }

  ngOnInit() {
    this.password = '';
    this.password2 = '';
  }

  changePassword() {
    this.userServ.savePassword(this.password).subscribe(
      ok => this.modalCtrl.dismiss({changed: true}),
      error => this.showToast(error, 3000)
    );
  }

  cancel() {
    this.modalCtrl.dismiss({ changed: false });
  }

  async showToast(message: string, duration: number) {
    const toast = await this.toast.create({
      message: message,
      duration: duration,
      position: 'bottom',
      color: 'dark'
    });

    await toast.present();
  }

}
