import { Component, OnInit, Input } from '@angular/core';
import { Iuser } from 'src/app/auth/interfaces/iuser';
import { ModalController, ToastController } from '@ionic/angular';
import { UsersService } from 'src/app/users/services/users.service';

@Component({
  selector: 'app-change-email',
  templateUrl: './change-email.component.html',
  styleUrls: ['./change-email.component.scss']
})
export class ChangeEmailComponent implements OnInit {
  @Input() user: Iuser;
  name: string;
  email: string;

  constructor(
    public modalCtrl: ModalController,
    public toast: ToastController,
    public userServ: UsersService
  ) { }

  ngOnInit() {
    this.name = this.user.name;
    this.email = this.user.email;
  }

  changeNameAndEmail() {
    this.userServ.saveProfile(this.name, this.email)
      .subscribe(
        ok => this.modalCtrl.dismiss({ changed: true, newName: this.name, newEmail: this.email }),
        error => this.showToast(error, 3000)
      );
  }

  cancel() {
    this.modalCtrl.dismiss({ changed: false });
  }

  async showToast(message: string, duration: number) {
    const toast = await this.toast.create({
      message: message,
      duration: duration,
      position: 'bottom',
      color: 'dark'
    });

    await toast.present();
  }

}
