import { Component, OnInit } from '@angular/core';
import { Iuser } from 'src/app/auth/interfaces/iuser';
import { Events, ModalController, NavController, ToastController } from '@ionic/angular';
import { UsersService } from 'src/app/users/services/users.service';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { ChangePasswordComponent } from './modals/change-password/change-password.component';
import { ChangeEmailComponent } from './modals/change-email/change-email.component';

@Component({
  selector: 'app-user-info',
  templateUrl: './user-info.page.html',
  styleUrls: ['./user-info.page.scss'],
})
export class UserInfoPage implements OnInit {
  user: Iuser;
  nuevoAvatar: string;

  constructor(
    private events: Events,
    private modalCtrl: ModalController,
    private nav: NavController,
    private camera: Camera,
    private toast: ToastController,
    private userService: UsersService
  ) { }

  ngOnInit() {
    this.events.subscribe('user', us => {
      this.user = us;
    });
  }

  changeAvatar() {
    this.userService.saveAvatar(this.nuevoAvatar).subscribe(
      async () => {
        (await this.toast.create({
          duration: 3000,
          position: 'bottom',
          message: 'Avatar changed!'
        })).present();
        this.user.avatar = this.nuevoAvatar;
        this.nuevoAvatar = '';
      }
    );
  }

  async takePhoto() {
    const options: CameraOptions = {
      targetWidth: 640, // max width 640px
      targetHeight: 640, // max height 640px
      allowEdit: true,
      destinationType: this.camera.DestinationType.DATA_URL // Base64
    };

    await this.getPicture(options);
  }

  async pickFromGallery() {
    const options: CameraOptions = {
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      targetWidth: 640, // max width 640px
      targetHeight: 640, // max height 640px
      allowEdit: true,
      destinationType: this.camera.DestinationType.DATA_URL // Base64
    };

    await this.getPicture(options);
  }

  private async getPicture(options: CameraOptions) {
    const imageData = await this.camera.getPicture(options);
    this.nuevoAvatar = 'data:image/jpeg;base64,' + imageData;
  }

  async openChangePassword() {
    const modal = await this.modalCtrl.create({
      component: ChangePasswordComponent,
      componentProps: {}
    });

    await modal.present();

    const result = await modal.onDidDismiss();
    if (result.data.changed) {
      this.showToast('Password updated successfully!', 3000);
    }
  }

  async openChangeNameEmail() {
    const modal = await this.modalCtrl.create({
      component: ChangeEmailComponent,
      componentProps: {user: this.user}
    });

    await modal.present();

    const result = await modal.onDidDismiss();
    if (result.data.changed) {
      this.showToast('Name and Email updated!', 3000);
      this.user.email = result.data.newEmail;
      this.user.name = result.data.newName;
    }
  }

  async showToast(message: string, duration: number) {
    const toast = await this.toast.create({
      message: message,
      duration: duration,
      position: 'bottom',
      color: 'dark'
    });

    await toast.present();
  }

}
