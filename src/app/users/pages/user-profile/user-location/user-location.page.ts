import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { Iuser } from 'src/app/auth/interfaces/iuser';
import { MapComponent } from 'ngx-mapbox-gl';
import { Events } from '@ionic/angular';

@Component({
  selector: 'app-user-location',
  templateUrl: './user-location.page.html',
  styleUrls: ['./user-location.page.scss'],
})
export class UserLocationPage implements OnInit, AfterViewInit {
  zoom = 17;
  user: Iuser;
  @ViewChild(MapComponent) mapComp: MapComponent;

  constructor(
    private events: Events
  ) { }

  ngAfterViewInit() {
    this.mapComp.load.subscribe(
      () => {
        console.log(this.mapComp);
        console.log(this.mapComp.mapInstance);
        this.mapComp.mapInstance.resize(); // Necessary for full height
      }
    );
  }

  ngOnInit() {
    this.events.subscribe('user', ususario => {
      this.user = ususario;
    });
  }
}
