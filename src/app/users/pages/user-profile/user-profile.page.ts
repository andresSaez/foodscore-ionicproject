import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Events } from '@ionic/angular';
import { Iuser } from 'src/app/auth/interfaces/iuser';

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.page.html',
  styleUrls: ['./user-profile.page.scss'],
})
export class UserProfilePage implements OnInit {
  user: Iuser;

  constructor(private route: ActivatedRoute, private events: Events) { }

  ngOnInit() {
    this.user = this.route.snapshot.data.user;
  }

  changeTab(event) {
    this.events.publish('user', this.user);
  }

}
